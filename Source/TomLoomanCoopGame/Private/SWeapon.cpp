// Fill out your copyright notice in the Description page of Project Settings.

#include "SWeapon.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/PlayerController.h"
#include "DrawDebugHelpers.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TomLoomanCoopGame.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

static int32 DebugWeaponDrawing = 0;
FAutoConsoleVariableRef CVARDebugWeaponDrawing (
	TEXT("COOP.DebugWeapons"), 
	DebugWeaponDrawing, 
	TEXT("Draw debug lines for weapons"), 
	ECVF_Cheat
);

// Sets default values
ASWeapon::ASWeapon()
{
	MeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;

	MuzzleSocketName = "MuzzleSocket";
	ShellEjectSocketName = "ShellEjectSocket";
	TracerTargetName = "Target";

	SetReplicates(true);
	NetUpdateFrequency = 66.0f;
	MinNetUpdateFrequency = 33.0f;

	BulletSpread = 10.0f;
}

void ASWeapon::BeginPlay()
{
	Super::BeginPlay();
	TimeBetweenShots = 60.0f / RoundsPerMinute;
	AmmoCurrent = AmmoMax;
}

void ASWeapon::BeginFire()
{
	const float FirstDelay = LastFireTime + TimeBetweenShots - GetWorld()->TimeSeconds;
	const float ClampFirstDelay = FMath::Max(FirstDelay, 0.0f);

	GetWorldTimerManager().SetTimer(
		TimerHandle_TimeBetweenShots,
		this,
		&ASWeapon::Fire,
		TimeBetweenShots,
		true,
		ClampFirstDelay
	);
}

void ASWeapon::EndFire()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_TimeBetweenShots);
}

void ASWeapon::ServerFire_Implementation()
{
	Fire();
}

bool ASWeapon::ServerFire_Validate()
{
	return true;
}

void ASWeapon::Fire()
{
	if (Role < ROLE_Authority)
	{
		ServerFire();
	}

	if (AmmoCurrent <= 0) { 
		Reload();
		return;
	}

	AActor* WeaponOwner = GetOwner();
	if (WeaponOwner == nullptr) { return; }

	FVector EyeLocation;
	FRotator EyeRotation;
	WeaponOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

	FVector ShotDirection = EyeRotation.Vector();
	
	// Bullet spread
	float HalfRad = FMath::DegreesToRadians(BulletSpread);
	ShotDirection = FMath::VRandCone(ShotDirection, HalfRad, HalfRad);

	FVector TraceEnd = EyeLocation + ShotDirection * 10000;
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(WeaponOwner);
	QueryParams.AddIgnoredActor(this);
	QueryParams.bReturnPhysicalMaterial = true; // Or material will be nullptr
	QueryParams.bTraceComplex = true; // Check each polygon on the way

	// Tracer particles end target;
	FVector TracerEndPoint = TraceEnd;

	EPhysicalSurface SurfaceType = SurfaceType_Default;
	FHitResult Hit;
	if (GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, COLLISION_WEAPON, QueryParams))
	{
		AActor* HitActor = Hit.GetActor();
		TracerEndPoint = Hit.ImpactPoint;

		// Weak object pointer here, so material can be deleted after usage
		SurfaceType = UPhysicalMaterial::DetermineSurfaceType(Hit.PhysMaterial.Get());

		if (HitActor)
		{
			float ActualDamage = SurfaceType == SURFACE_FLESHVULNERABLE
				? BaseDamage * VulnerableDamageMultiplier
				: BaseDamage;

			UGameplayStatics::ApplyPointDamage(
				HitActor, 
				ActualDamage, 
				ShotDirection, 
				Hit, 
				WeaponOwner->GetInstigatorController(), 
				WeaponOwner,
				DamageType
			);
		}

		PlayImpactEffect(SurfaceType, Hit.ImpactPoint);
	}

	if (DebugWeaponDrawing > 0)
	{
		DrawDebugLine(GetWorld(), EyeLocation, TracerEndPoint, FColor::White, false, 2.f, 0, 2.f);
	}

	if (Role == ROLE_Authority)
	{
		HitScanTrace.TraceEnd = TracerEndPoint;
		HitScanTrace.SurfaceType = SurfaceType;
		HitScanTrace.ShootPingPong = !HitScanTrace.ShootPingPong;
		--AmmoCurrent;
	}

	PlayMuzzleEffect();
	PlayCameraShake();
	PlayTracerEffect(TracerEndPoint);
	PlayShellEjectEffect();

	LastFireTime = GetWorld()->TimeSeconds;
}

void ASWeapon::OnRep_HitScanTrace()
{
	// Play cosmetic FX
	PlayTracerEffect(HitScanTrace.TraceEnd);
	PlayImpactEffect(HitScanTrace.SurfaceType, HitScanTrace.TraceEnd);
}


void ASWeapon::PlayMuzzleEffect()
{
	if (MuzzleEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleEffect, MeshComp, MuzzleSocketName);
	}
}

void ASWeapon::PlayImpactEffect(EPhysicalSurface PhysicalSurface, FVector ImpactPoint)
{
	UParticleSystem* SelectedEffect;

	switch (PhysicalSurface) {
	case SURFACE_FLESHDEFAULT:
	case SURFACE_FLESHVULNERABLE:
		SelectedEffect = FleshImpactEffect;
		break;
	default:
		SelectedEffect = DefaultImpactEffect;
		break;
	}

	if (SelectedEffect)
	{
		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
		FVector ShotDirection = ImpactPoint - MuzzleLocation;
		ShotDirection.Normalize();

		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SelectedEffect, ImpactPoint, ShotDirection.Rotation());
	}
}

void ASWeapon::PlayTracerEffect(const FVector& TracerEndPoint)
{
	if (TracerEffect)
	{
		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
		UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), TracerEffect, MuzzleLocation);
		if (TracerComp)
		{
			TracerComp->SetVectorParameter(TracerTargetName, TracerEndPoint);
		}
	}
}

void ASWeapon::PlayShellEjectEffect()
{
	if (ShellEjectEffect)
	{
		UGameplayStatics::SpawnEmitterAttached(ShellEjectEffect, MeshComp, ShellEjectSocketName);
	}
}

void ASWeapon::PlayCameraShake()
{
	APawn* WeaponOwner = Cast<APawn>(GetOwner());
	if (WeaponOwner)
	{
		APlayerController* OwnerController = Cast<APlayerController>(WeaponOwner->GetController());
		Maybe(OwnerController)->ClientPlayCameraShake(FireCameraShake);
	}
}

void ASWeapon::Reload()
{
	if (!GetWorldTimerManager().IsTimerActive(TimerHandle_ReloadTime))
	{
		GetWorldTimerManager().SetTimer(
			TimerHandle_ReloadTime,
			this,
			&ASWeapon::OnReloadFinished,
			ReloadTime,
			true
		);
	}
}

void ASWeapon::OnReloadFinished()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_ReloadTime);
	AmmoCurrent = AmmoMax;
	OnReload();
}

float ASWeapon::GetReloadTimeLeft() const
{
	const float TimeRemain = GetWorldTimerManager().GetTimerRemaining(TimerHandle_ReloadTime);
	return TimeRemain >= 0.0f ? TimeRemain : 0.0f ;
}

void ASWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASWeapon, AmmoCurrent);
	DOREPLIFETIME_CONDITION(ASWeapon, HitScanTrace, COND_SkipOwner);
}