// Fill out your copyright notice in the Description page of Project Settings.

#include "SGameMode.h"
#include "SGameState.h"
#include "SPlayerState.h"
#include "Components/SHealthComponent.h"

#include "TimerManager.h"
#include "Engine/World.h"


ASGameMode::ASGameMode()
{
	GameStateClass = ASGameState::StaticClass();
	PlayerStateClass = ASPlayerState::StaticClass();

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickInterval = 1.0f;
}

void ASGameMode::StartPlay()
{
	Super::StartPlay();

	PrepareNextWave();
}

void ASGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CheckWaveState();
	CheckAnyPlayerAlive();
}

void ASGameMode::SpawnBotTimerElapsed()
{
	SpawnBot();

	--NumberOfBotsToSpawn;

	if (NumberOfBotsToSpawn <= 0)
	{
		EndWave();

		SetWaveState(EWaveState::WaitingToWaveComplete);
	}
}

void ASGameMode::StartWave()
{
	++WaveCount;
	NumberOfBotsToSpawn = 2 * WaveCount;

	GetWorldTimerManager().SetTimer(
		TimerHandle_BotSpawner,
		this,
		&ASGameMode::SpawnBotTimerElapsed,
		BotSpawnInterval,
		true,
		0.0f
	);

	SetWaveState(EWaveState::WaveInProgress);
}

void ASGameMode::EndWave()
{
	GetWorldTimerManager().ClearTimer(TimerHandle_BotSpawner);
}

void ASGameMode::GameOver()
{
	EndWave();

	SetWaveState(EWaveState::GameOver);

	UE_LOG(LogTemp, Log, TEXT("All players died"));
}

void ASGameMode::CheckWaveState()
{
	// Already spawning bots
	if (NumberOfBotsToSpawn > 0)
	{
		return;
	}

	// Already preparing next wave
	if (GetWorldTimerManager().IsTimerActive(TimerHandle_NextWaveStart))
	{
		return;
	}

	bool bAllBotsDead = true;

	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; It++)
	{
		APawn* TestPawn = It->Get();
		if (TestPawn == nullptr || TestPawn->IsPlayerControlled())
		{
			continue;
		}

		USHealthComponent* HealthComp = Cast<USHealthComponent>(TestPawn->GetComponentByClass(USHealthComponent::StaticClass()));
		if (HealthComp && HealthComp->GetHealth() > 0.0f)
		{
			bAllBotsDead = false;
			break;
		}
	}

	if (bAllBotsDead)
	{
		PrepareNextWave();
	}
}

void ASGameMode::CheckAnyPlayerAlive()
{
	bool bAllPlayersDead = true;

	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; It++)
	{
		APlayerController* PlayerController = It->Get();
		if (PlayerController == nullptr)
		{
			continue;
		}

		APawn* TestPawn = PlayerController->GetPawn();
		if (TestPawn == nullptr)
		{
			continue;
		}

		USHealthComponent* HealthComp = Cast<USHealthComponent>(TestPawn->GetComponentByClass(USHealthComponent::StaticClass()));
		// ensure throws error and breakes game if character has no HealthComp
		if (ensure(HealthComp) && HealthComp->GetHealth() > 0.0f)
		{
			bAllPlayersDead = false;
			return;
		}
	}

	GameOver();
}

void ASGameMode::PrepareNextWave()
{
	GetWorldTimerManager().SetTimer(
		TimerHandle_NextWaveStart,
		this,
		&ASGameMode::StartWave,
		WaveInterval,
		false
	);

	SetWaveState(EWaveState::PreparingNextWave);
}

void ASGameMode::RespawnDeadPlayers()
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; It++)
	{
		APlayerController* PlayerController = It->Get();
		if (PlayerController && PlayerController->GetPawn() == nullptr)
		{
			RestartPlayer(PlayerController);
		}
	}
}

void ASGameMode::SetWaveState(EWaveState NewWaveState)
{
	ASGameState* GameState = GetGameState<ASGameState>();
	if (ensureAlways(GameState))
	{
		GameState->SetWaveState(NewWaveState);
	}
}
