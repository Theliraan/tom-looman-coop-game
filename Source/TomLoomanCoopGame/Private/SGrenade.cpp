// Fill out your copyright notice in the Description page of Project Settings.

#include "SGrenade.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TomLoomanCoopGame.h"

ASGrenade::ASGrenade()
{
	RadialForce = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForce"));
	RadialForce->SetupAttachment(RootComponent);
	RadialForce->bAutoActivate = false;
	RadialForce->bImpulseVelChange = true; // to throw actors to the air!
	RadialForce->ForceStrength = ForceStrength;
	RadialForce->ImpulseStrength = ImpulseStrength;
	RadialForce->Radius = ForceRadius;
}

void ASGrenade::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
}

void ASGrenade::Destroyed()
{
	Super::Destroyed();
	
	RadialForce->FireImpulse();
	RadialForce->Activate();

	TArray<AActor*> IgnoreActors;
	UGameplayStatics::ApplyRadialDamage(
		this, 
		Damage, 
		GetActorLocation(), 
		DamageRadius, 
		DamageType, 
		IgnoreActors,
		this,
		GetInstigatorController(),
		false
	);

	if (ImpactEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, GetActorTransform());
	}
}