// Fill out your copyright notice in the Description page of Project Settings.

#include "SPowerUpActor.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASPowerUpActor::ASPowerUpActor()
{
	TickInterval = 0.0f;
	NumberOfTicks = 0;
	InstantTick = false;
	bIsActivate = false;

	SetReplicates(true);
}

void ASPowerUpActor::Activate(AActor* ActivateFor)
{
	bIsActivate = true;
	OnRep_PowerUpActivate();

	OnActivated(ActivateFor);

	if (TickInterval == 0.0f || NumberOfTicks == 0)
	{
		OnTickPowerUp();
		return;
	}

	CurrentTick = 0;

	GetWorldTimerManager().SetTimer(
		TimerHandle_PowerUpTick,
		this, &ASPowerUpActor::OnTickPowerUp,
		TickInterval,
		true,
		InstantTick ? 0.0f : TickInterval
	);
}

void ASPowerUpActor::OnRep_PowerUpActivate()
{
	OnPowerUpStateChanged(bIsActivate);
}

void ASPowerUpActor::OnTickPowerUp()
{
	++CurrentTick;
	OnTicked();

	if (CurrentTick >= NumberOfTicks) {
		bIsActivate = false;
		OnRep_PowerUpActivate();

		GetWorldTimerManager().ClearTimer(TimerHandle_PowerUpTick);
	}
}

void ASPowerUpActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASPowerUpActor, bIsActivate);
}