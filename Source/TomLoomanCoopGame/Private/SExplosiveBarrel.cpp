// Fill out your copyright notice in the Description page of Project Settings.

#include "SExplosiveBarrel.h"
#include "Components/SHealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "TomLoomanCoopGame.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ASExplosiveBarrel::ASExplosiveBarrel()
{
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	MeshComp->SetSimulatePhysics(true);
	//MeshComp->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	RootComponent = MeshComp;

	HealthComp = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComp"));

	RadialForce = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForce"));
	RadialForce->SetupAttachment(RootComponent);
	RadialForce->bAutoActivate = false;
	RadialForce->bImpulseVelChange = true; // to throw actors to the air!
	RadialForce->ImpulseStrength = ImpulseStrength;
	RadialForce->Radius = ImpulseRadius;
	RadialForce->bIgnoreOwningActor = true;

	bDied = false;

	SetReplicates(true);
	SetReplicateMovement(true);
}

// Called when the game starts or when spawned
void ASExplosiveBarrel::BeginPlay()
{
	Super::BeginPlay();

	if (Role == ROLE_Authority)
	{
		HealthComp->OnHealthChanged.AddDynamic(this, &ASExplosiveBarrel::OnHealthChanged);
	}
}

void ASExplosiveBarrel::OnHealthChanged(
	USHealthComponent* HealthComp,
	float Health,
	float HealthDelta,
	const class UDamageType* TakenDamageType,
	class AController* InstigatedBy,
	AActor* DamageCauser
)
{
	if (!bDied && Health <= 0.0f)
	{
		bDied = true;
		Explode();
	}
}

void ASExplosiveBarrel::Explode()
{
	HealthComp->Deactivate();

	MeshComp->SetMaterial(0, ExplodeMaterial);

	// Throw to the air
	if (FlingImpulse > 0.0f)
	{
		MeshComp->AddImpulse(FVector::UpVector * FlingImpulse, NAME_None, true);
	}

	RadialForce->FireImpulse();
	RadialForce->Activate();

	if (Damage > 0.0f) 
	{
		TArray<AActor*> IgnoreActors;
		UGameplayStatics::ApplyRadialDamage(
			this,
			Damage,
			GetActorLocation(),
			DamageRadius,
			DamageType,
			IgnoreActors,
			this,
			GetInstigatorController(),
			false
		);
	}

	if (ExplosionEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorTransform());
	}
}

void ASExplosiveBarrel::OnRep_Died()
{
	MeshComp->SetMaterial(0, ExplodeMaterial);

	if (ExplosionEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorTransform());
	}
}

void ASExplosiveBarrel::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(ASExplosiveBarrel, bDied, COND_SkipOwner);
}