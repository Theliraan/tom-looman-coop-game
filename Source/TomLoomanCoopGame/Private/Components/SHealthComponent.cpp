// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/SHealthComponent.h"
#include "GameFramework/Actor.h"
#include "Net/UnrealNetwork.h"
#include "SGameMode.h"
#include "Engine/World.h"

// Sets default values for this component's properties
USHealthComponent::USHealthComponent()
{
	DefaultHealth = 100.0f;
	uint8 TeamNumber = 255;

	// Componens have no SetReplicates field - only actors
	SetIsReplicated(true);
}

// Called when the game starts
void USHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// Componens have no Role field - only actors
	if (GetOwnerRole() == ROLE_Authority)
	{
		AActor* ComponentOwner = GetOwner();
		if (ComponentOwner)
		{
			ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &USHealthComponent::HandleTakeAnyDamage);
		}
	}

	Health = DefaultHealth;
}

void USHealthComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.0f || Health <= 0.0f)
	{
		return;
	}

	if (IsFriendly(DamagedActor, DamageCauser))
	{
		return;
	}

	float HealthDelta = FMath::Clamp(Damage, 0.0f, Health);
	Health -= HealthDelta;

	OnHealthChanged.Broadcast(this, Health, HealthDelta, DamageType, InstigatedBy, DamageCauser);

	if (Health <= 0.0f)
	{
		ASGameMode* GameMode = Cast<ASGameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnActorKilled.Broadcast(DamagedActor, DamageCauser, InstigatedBy);
		}
	}
	//UE_LOG(LogTemp, Warning, TEXT("Health changed: %s"), *FString::SanitizeFloat(Health, 3));
}

float USHealthComponent::GetHealth() const
{
	return Health;
}

void USHealthComponent::Heal(float HealAmount)
{
	if (HealAmount <= 0.0f || Health <= 0.0f)
	{
		return;
	}

	Health = FMath::Clamp(Health + HealAmount, 0.0f, DefaultHealth);

	OnHealthChanged.Broadcast(this, Health, HealAmount, nullptr, nullptr, nullptr);
}

void USHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USHealthComponent, Health);
}


void USHealthComponent::OnRep_Health(float OldHealth)
{
	float HealthDelta = Health - OldHealth;

	OnHealthChanged.Broadcast(this, Health, HealthDelta, nullptr, nullptr, nullptr);
}

bool USHealthComponent::IsFriendly(AActor * ActorA, AActor * ActorB)
{
	if (ActorA == nullptr || ActorB == nullptr)
	{
		return true;
	}

	if (ActorA == ActorB)
	{
		return false;
	}

	USHealthComponent* CompA = Cast<USHealthComponent>(ActorA->GetComponentByClass(USHealthComponent::StaticClass()));
	if (CompA == nullptr)
	{
		return true;
	}

	USHealthComponent* CompB = Cast<USHealthComponent>(ActorB->GetComponentByClass(USHealthComponent::StaticClass()));
	if (CompB == nullptr)
	{
		return true;
	}

	return CompA->TeamNumber == CompB->TeamNumber;
}