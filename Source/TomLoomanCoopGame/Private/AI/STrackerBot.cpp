// Fill out your copyright notice in the Description page of Project Settings.

#include "AI/STrackerBot.h"

#include "NavigationSystem/Public/NavigationSystem.h"
#include "NavigationSystem/Public/NavigationPath.h"

#include "Components/SphereComponent.h"
#include "Components/SHealthComponent.h"
#include "Components/StaticMeshComponent.h"

#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Engine/World.h"
#include "Sound/SoundCue.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"

#include "SCharacter.h"
#include "../TomLoomanCoopGame.h"

#include "DrawDebugHelpers.h"

static int32 DebugTrackerBotDrawing = 0;
FAutoConsoleVariableRef CVARDebugTrackerBotDrawing(
	TEXT("COOP.DebugTrackerBots"),
	DebugTrackerBotDrawing,
	TEXT("Draw debug gizmos for tracker bots"),
	ECVF_Cheat
);

// Sets default values
ASTrackerBot::ASTrackerBot()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	MeshComp->SetCanEverAffectNavigation(false);
	MeshComp->SetSimulatePhysics(true);
	RootComponent = MeshComp;
	
	HealthComp = CreateDefaultSubobject<USHealthComponent>(TEXT("HealthComp"));
	HealthComp->OnHealthChanged.AddDynamic(this, &ASTrackerBot::OnHealthChanged);

	MovementForce = 600.0f;
	RequiredDistanceToTarget = 100.0f;
	bUseVelocityChange = true;
	MaterialDamageTimeName = FName("LastTimeDamageTaken");
	PowerLevelAlphaName = FName("PowerLevelAlpha");

	bExploded = false;
	CountTrackersInRadius = 0;
	ExplosionDamageAddition = 1.0f / 3.0f;
	ExplosionDamage = 60.0f;
	ExplosionRadius = 400.0f;

	SelfExplodeRadius = 100.0f;
	SelfDamageInterval = 0.5f;
	bStartedSelfDestruction = false;

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	SphereComp->SetSphereRadius(SelfExplodeRadius);
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComp->SetCollisionObjectType(ECC_Vehicle);
	SphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	SphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	SphereComp->SetCollisionResponseToChannel(ECC_Vehicle, ECR_Overlap);
	SphereComp->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASTrackerBot::BeginPlay()
{
	Super::BeginPlay();
	
	if (Role == ROLE_Authority)
	{
		NextPathPoint = GetNextPathPoint();
	}

	MatInst = MeshComp->CreateAndSetMaterialInstanceDynamicFromMaterial(0, MeshComp->GetMaterial(0));
}

// Called every frame
void ASTrackerBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Role != ROLE_Authority || bExploded)
	{
		return;
	}

	float DistanceToTarget = (GetActorLocation() - NextPathPoint).Size();

	if (DistanceToTarget <= RequiredDistanceToTarget)
	{
		NextPathPoint = GetNextPathPoint();

		if (DebugTrackerBotDrawing > 0)
		{
			DrawDebugString(GetWorld(), GetActorLocation(), "Target reached!");
		}
	}
	else
	{
		FVector ForceDirection = NextPathPoint - GetActorLocation();
		ForceDirection.Normalize();
		ForceDirection *= MovementForce;
		MeshComp->AddForce(ForceDirection, NAME_None, bUseVelocityChange);

		if (DebugTrackerBotDrawing > 0)
		{
			DrawDebugDirectionalArrow(
				GetWorld(),
				GetActorLocation(),
				GetActorLocation() + ForceDirection,
				32.0f,
				FColor::Red,
				false,
				0.0f,
				0u,
				2.0f
			);
		}
	}

	if (DebugTrackerBotDrawing > 0)
	{
		DrawDebugSphere(
			GetWorld(), 
			GetActorLocation(), 
			20.0f, 
			32.0f, 
			FColor::Red, 
			false, 
			0.0f, 
			0u, 
			2.0f
		);
	}
}

// Called every frame
FVector ASTrackerBot::GetNextPathPoint()
{
	APawn* BestTarget = nullptr;
	float BestDistance = FLT_MAX;

	for (FConstPawnIterator It = GetWorld()->GetPawnIterator(); It; It++)
	{
		APawn* TestPawn = It->Get();
		if (TestPawn == nullptr || !TestPawn->IsPlayerControlled())
		{
			continue;
		}

		USHealthComponent* TestHealthComp = Cast<USHealthComponent>(TestPawn->GetComponentByClass(USHealthComponent::StaticClass()));
		if (TestHealthComp == nullptr || TestHealthComp->GetHealth() <= 0.0f || TestHealthComp->IsFriendly(this, TestPawn))
		{
			continue;
		}

		float CurrentDistance = (TestPawn->GetActorLocation() - GetActorLocation()).Size();		
		if (BestTarget == nullptr || CurrentDistance < BestDistance)
		{
			BestTarget = TestPawn;
			BestDistance = CurrentDistance;
		}
	}

	if (BestTarget) {
		UNavigationPath* NavPath = UNavigationSystemV1::FindPathToActorSynchronously(
			this,
			GetActorLocation(),
			BestTarget
		);

		GetWorldTimerManager().ClearTimer(TimeHandle_RefreshPath);
		GetWorldTimerManager().SetTimer(
			TimeHandle_RefreshPath,
			this, 
			&ASTrackerBot::RefreshPath,
			5.0f,
			false
		);

		if (NavPath && NavPath->PathPoints.Num() > 1)
		{
			// 0 is mine point, 1 - next
			return NavPath->PathPoints[1];
		}
	}

	return GetActorLocation();
}

void ASTrackerBot::RefreshPath()
{
	NextPathPoint = GetNextPathPoint();
}

void ASTrackerBot::OnHealthChanged(USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* TakenDamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	Maybe(MatInst)->SetScalarParameterValue(MaterialDamageTimeName, GetWorld()->TimeSeconds);

	if (!bExploded && Health <= 0.0f)
	{
		bExploded = true;
		Explode();
	}

	//UE_LOG(LogTemp, Log, TEXT("Health: %s of %s", *FString::SanitizeFloat(Health), *GetName()));
}

void ASTrackerBot::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (!bExploded)
	{
		if (!bStartedSelfDestruction)
		{
			ASCharacter* PlayerCharacter = Cast<ASCharacter>(OtherActor);
			if (PlayerCharacter && !USHealthComponent::IsFriendly(this, OtherActor))
			{
				// Start self-destruction sequence
				bStartedSelfDestruction = true;

				UGameplayStatics::SpawnSoundAttached(ExplodeWarningSound, RootComponent);

				if (Role == ROLE_Authority)
				{
					GetWorldTimerManager().SetTimer(
						TimeHandle_SelfDamage,
						this, &ASTrackerBot::DamageSelf,
						SelfDamageInterval,
						true,
						0.0f);
				}
				return;
			}
		}

		if (Role == ROLE_Authority && Cast<ASTrackerBot>(OtherActor) != nullptr)
		{
			++CountTrackersInRadius;
			VisualizePowerLevel();
		}
	}
}

void ASTrackerBot::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	if (Role == ROLE_Authority && Cast<ASTrackerBot>(OtherActor) != nullptr)
	{
		--CountTrackersInRadius;
		VisualizePowerLevel();
	}
}

void ASTrackerBot::OnRep_CountTrackersInRadius()
{
	VisualizePowerLevel();
}

void ASTrackerBot::VisualizePowerLevel()
{
	float PowerLevelAlpha = FMath::Clamp(ExplosionDamageAddition * CountTrackersInRadius, 0.0f, 1.0f);
	Maybe(MatInst)->SetScalarParameterValue(PowerLevelAlphaName, PowerLevelAlpha);
}

void ASTrackerBot::DamageSelf()
{
	UGameplayStatics::ApplyDamage(this, 20.0f, GetInstigatorController(), this, ExplosionDamageType);
}

void ASTrackerBot::Explode()
{
	if (ExplosionEffect)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionEffect, GetActorTransform());
	}

	if (ExplodeSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ExplodeSound, GetActorLocation());
	}

	// Workaround to stay alive to show explosion on client
	MeshComp->SetVisibility(false, true);
	MeshComp->SetSimulatePhysics(false);
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComp->SetCollisionResponseToAllChannels(ECR_Ignore);

	if (Role == ROLE_Authority)
	{
		TArray<AActor*> IgnoredActors;
		IgnoredActors.Add(this);

		float TotalDamage = ExplosionDamage + 
			// Additional for each tracker bot in sense-radius
			ExplosionDamage * ExplosionDamageAddition * CountTrackersInRadius;

		UGameplayStatics::ApplyRadialDamage(
			this, 
			TotalDamage,
			GetActorLocation(), 
			ExplosionRadius,
			ExplosionDamageType, // can be nullptr
			IgnoredActors, // can be TArray<AActor*>::TArray()
			this,
			GetInstigatorController(),
			true,
			COLLISION_WEAPON
		);

		// Workaround to stay alive to show explosion on client
		SetLifeSpan(2.0f);
	}
}

void ASTrackerBot::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASTrackerBot, CountTrackersInRadius);
}