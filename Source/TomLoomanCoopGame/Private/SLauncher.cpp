// Fill out your copyright notice in the Description page of Project Settings.

#include "SLauncher.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"

void ASLauncher::Fire()
{
	if (AmmoCurrent <= 0) {
		Reload();
		return;
	}

	AActor* WeaponOwner = GetOwner();
	if (WeaponOwner == nullptr) { return; }

	FVector EyeLocation;
	FRotator EyeRotation;
	WeaponOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);
		
	FVector ShotDirection = EyeRotation.Vector();
	FVector ShotLocation = MeshComp->GetSocketLocation(MuzzleSocketName);
	AActor* Projectile = GetWorld()->SpawnActor<AActor>(ProjectileType, ShotLocation, EyeRotation);
		
	PlayMuzzleEffect();
	PlayShellEjectEffect();
	PlayCameraShake();
	--AmmoCurrent;
}


