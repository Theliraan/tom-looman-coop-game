// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

// Game modes 
#define SURFACE_FLESHDEFAULT	SurfaceType1
#define SURFACE_FLESHVULNERABLE	SurfaceType2

#define COLLISION_WEAPON		ECC_GameTraceChannel1

// Null propagation / Maybe
#define Maybe(X)				(X==nullptr) ? nullptr : X

// Unreal
#define UELOG(X)				UE_LOG(LogTemp, Info, TEXT(X))
#define UELOGW(X)				UE_LOG(LogTemp, Warning, TEXT(X))
#define UELOGE(X)				UE_LOG(LogTemp, Error, TEXT(X))