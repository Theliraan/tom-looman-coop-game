// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SPowerUpActor.generated.h"

UCLASS()
class TOMLOOMANCOOPGAME_API ASPowerUpActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASPowerUpActor();

	UPROPERTY(ReplicatedUsing = OnRep_PowerUpActivate)
	bool bIsActivate;

	UFUNCTION()
	void OnRep_PowerUpActivate();

	UFUNCTION(BlueprintImplementableEvent, Category = "Powerups")
	void OnPowerUpStateChanged(bool bIsNewActive);

	void Activate(AActor* ActivateFor);

	UFUNCTION(BlueprintImplementableEvent, Category = "Powerups")
	void OnActivated(AActor* ActivateFor);

	UFUNCTION(BlueprintImplementableEvent, Category = "Powerups")
	void OnExpired();

	UFUNCTION(BlueprintImplementableEvent, Category = "Powerups")
	void OnTicked();

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Powerups")
	bool InstantTick;

	UPROPERTY(EditDefaultsOnly, Category = "Powerups")
	float TickInterval;

	UPROPERTY(EditDefaultsOnly, Category = "Powerups")
	int NumberOfTicks;

	int CurrentTick;

	UFUNCTION()
	void OnTickPowerUp();
	
	FTimerHandle TimerHandle_PowerUpTick;
};
