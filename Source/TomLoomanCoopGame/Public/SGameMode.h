// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SGameMode.generated.h"

enum class EWaveState : uint8;

// Killed actor, killer actor
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnActorKilled, AActor*, VictimActor, AActor*, KillerActor, AController*, KillerController);

/**
 * 
 */
UCLASS()
class TOMLOOMANCOOPGAME_API ASGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void SpawnBot();

	void SpawnBotTimerElapsed();

	void StartWave();

	void EndWave();

	void GameOver();

	void CheckWaveState();

	void CheckAnyPlayerAlive();

	void PrepareNextWave();

	void RespawnDeadPlayers();

	void SetWaveState(EWaveState NewWaveState);

	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	float BotSpawnInterval = 1.0f;

	// Number of bots to spawn in current wave
	int NumberOfBotsToSpawn;

	UPROPERTY(EditDefaultsOnly, Category = "GameMode")
	float WaveInterval = 10.0f;

	int WaveCount = 0;

	FTimerHandle TimerHandle_BotSpawner;

	FTimerHandle TimerHandle_NextWaveStart;

public:

	ASGameMode();

	virtual void StartPlay() override;

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintAssignable, Category = "GameMode")
	FOnActorKilled OnActorKilled;
};
