// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SGameState.generated.h"

UENUM()
enum class EWaveState : uint8
{
	WaitingToStart,
	PreparingNextWave,
	WaveInProgress,
	WaitingToWaveComplete,
	GameOver,
};

/**
 * 
 */
UCLASS()
class TOMLOOMANCOOPGAME_API ASGameState : public AGameStateBase
{
	GENERATED_BODY()

public:

	void SetWaveState(EWaveState NewWaveState);
	
protected:

	UPROPERTY(ReplicatedUsing = OnRep_WaveState, BlueprintReadOnly, Category = "GameState")
	EWaveState WaveState;

	UFUNCTION()
	void OnRep_WaveState(EWaveState OldWaveState);

	UFUNCTION(BlueprintImplementableEvent, Category = "GameState")
	void WaveStateChanged(EWaveState NewWaveState, EWaveState OldWaveState);
};
