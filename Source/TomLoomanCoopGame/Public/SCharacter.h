// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "SCharacter.generated.h"

class USHealthComponent;
class UCameraComponent;
class USpringArmComponent;
class ASWeapon;

UCLASS()
class TOMLOOMANCOOPGAME_API ASCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void BeginCrouch();
	void EndCrouch();

	void BeginJump();
	void EndJump();

	void BeginZoom();
	void EndZoom();

	void MoveForward(float Value);
	void MoveRight(float Value);

	void Reload();

	/* Pawn died previously */
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Health")
	bool bDied;

	UFUNCTION()
	void OnHealthChanged(USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health")	
	USHealthComponent* HealthComp;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Components")
	FName WeaponSocketName;

	UPROPERTY(EditAnywhere, Category = "Zoom")
	float bWantsToZoom = false;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Zoom")
	float ZoomedFOV = 60.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Zoom", meta = (ClampMin = 0.1, ClampMax = 100))
	float ZoomInterpSpeed = 20.f;

	float DefaultFOV;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Weapon")
	ASWeapon* CurrentWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<ASWeapon> StarterWeaponClass;

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void SwapWeapon(TSubclassOf<ASWeapon> WeaponType);

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual FVector GetPawnViewLocation() const override;

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void BeginFire();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void EndFire();
};
