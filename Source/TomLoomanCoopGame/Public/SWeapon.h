// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SWeapon.generated.h"

class USkeletalMeshComponent;
class UDamageType;
class UParticleSystem;
class UCameraShake;

// Contains information of single hitscan weapon line-trace
USTRUCT()
struct FHitScanTrace
{
	GENERATED_BODY()

public:

	UPROPERTY()
	bool ShootPingPong;

	UPROPERTY()
	TEnumAsByte<EPhysicalSurface> SurfaceType;

	UPROPERTY()
	FVector_NetQuantize TraceEnd;
};

UCLASS()
class TOMLOOMANCOOPGAME_API ASWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASWeapon();

	UPROPERTY(VisibleAnywhere, BlueprintReadonly, Category = "Components")
	USkeletalMeshComponent* MeshComp;

	void BeginFire();
	void EndFire();

	void Reload();

	UFUNCTION(BlueprintCallable, Category = "Ammo")
	float GetReloadTimeLeft() const;

protected:

	virtual void BeginPlay() override;

	// CATEGORY: AMMO

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Ammo")
	int AmmoCurrent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammo")
	int AmmoMax = 30;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammo")
	float ReloadTime = 1.0f;

	void OnReloadFinished();

	UFUNCTION(BlueprintImplementableEvent, Category = "Ammo")
	void OnReload();

	FTimerHandle TimerHandle_ReloadTime;

	// CATEGORY: FIRE

	virtual void Fire();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerFire();

	float LastFireTime = 0.0f;

	FTimerHandle TimerHandle_TimeBetweenShots;

	UPROPERTY(ReplicatedUsing = OnRep_HitScanTrace)
	FHitScanTrace HitScanTrace;

	UFUNCTION()
	void OnRep_HitScanTrace();

	// CATEGORY: DAMAGE

	float TimeBetweenShots;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float RoundsPerMinute = 300.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float BaseDamage = 20.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	float VulnerableDamageMultiplier = 4.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = "Damage")
	TSubclassOf<UDamageType> DamageType;

	/* Bullet spread in degrees */
	UPROPERTY(EditDefaultsOnly, BlueprintReadonly, Category = "Damage", meta = (ClampMin=0.0f, ClampMax = 90.0f))
	float BulletSpread;

	// CATEGORY: FX SHELL EJECT

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	UParticleSystem* ShellEjectEffect;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "FX")
	FName ShellEjectSocketName;

	void PlayShellEjectEffect();

	// CATEGORY: FX MUZZLE

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	UParticleSystem* MuzzleEffect;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "FX")
	FName MuzzleSocketName;

	void PlayMuzzleEffect();

	// CATEGORY: FX IMPACT

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	UParticleSystem* DefaultImpactEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	UParticleSystem* FleshImpactEffect;

	void PlayImpactEffect(EPhysicalSurface SurfaceType, FVector ImpactPoint);

	// CATEGORY: FX TRACER

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "FX")
	UParticleSystem* TracerEffect;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "FX")
	FName TracerTargetName;

	void PlayTracerEffect(const FVector& TracerEndPoint);

	// CATEGORY: FX CAMERA SHAKE

	UPROPERTY(EditDefaultsOnly, Category = "FX")
	TSubclassOf<UCameraShake> FireCameraShake;

	void PlayCameraShake();

public:	
	
};
