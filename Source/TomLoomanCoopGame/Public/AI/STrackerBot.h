// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "STrackerBot.generated.h"

class UStaticMeshComponent;
class USHealthComponent;
class UMaterialInstanceDynamic;
class UParticleSystem;
class UDamageType;
class USphereComponent;
class USoundCue;

UCLASS()
class TOMLOOMANCOOPGAME_API ASTrackerBot : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASTrackerBot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	UStaticMeshComponent* MeshComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	USphereComponent* SphereComp;

	UPROPERTY(VisibleDefaultsOnly, Category = "Components")
	USHealthComponent* HealthComp;

	FVector GetNextPathPoint();

	FVector NextPathPoint;

	UPROPERTY(EditDefaultsOnly, Category = "TrackerBot")
	float MovementForce;

	UPROPERTY(EditDefaultsOnly, Category = "TrackerBot")
	bool bUseVelocityChange;

	UPROPERTY(EditDefaultsOnly, Category = "TrackerBot")
	float RequiredDistanceToTarget;
	
	UFUNCTION()
	void OnHealthChanged(USHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* TakenDamageType, class AController* InstigatedBy, AActor* DamageCauser);
		
	UPROPERTY(BlueprintReadonly, Category = "Materials")
	FName MaterialDamageTimeName;

	UPROPERTY(BlueprintReadonly, Category = "Materials")
	FName PowerLevelAlphaName;

	FTimerHandle TimeHandle_RefreshPath;

	void RefreshPath();

	// Dynamic material to pulse on damage
	UMaterialInstanceDynamic* MatInst;

	void Explode();

	bool bExploded;

	UPROPERTY(ReplicatedUsing = OnRep_CountTrackersInRadius, BlueprintReadOnly, Category = "Explosion")
	int CountTrackersInRadius;

	UFUNCTION()
	void OnRep_CountTrackersInRadius();

	// Update material glowing
	void VisualizePowerLevel();

	UPROPERTY(EditDefaultsOnly, Category = "Explosion")
	UParticleSystem* ExplosionEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Explosion")
	TSubclassOf<UDamageType> ExplosionDamageType;

	// Damage addition multiplier for each bot in sense-radius
	UPROPERTY(EditDefaultsOnly, Category = "Explosion")
	float ExplosionDamageAddition;

	UPROPERTY(EditDefaultsOnly, Category = "Explosion")
	float ExplosionDamage;

	UPROPERTY(EditDefaultsOnly, Category = "Explosion")
	float ExplosionRadius;

	UPROPERTY(EditDefaultsOnly, Category = "Explosion")
	float SelfExplodeRadius;

	UPROPERTY(EditDefaultsOnly, Category = "Explosion")
	float SelfDamageInterval;

	bool bStartedSelfDestruction;

	FTimerHandle TimeHandle_SelfDamage;

	void DamageSelf();

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	USoundCue* ExplodeWarningSound;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	USoundCue* ExplodeSound;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;
	
};
